import 'package:flutter/material.dart';
import 'package:pakistani_app/config/asset_paths.dart';
import 'package:pakistani_app/data/dummy.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/base_widget.dart';
import 'package:pakistani_app/widgets/gap.dart';
import 'package:pakistani_app/widgets/text.dart';

class ScreenDetail extends StatelessWidget {
  const ScreenDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      alignCenter: false,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(AssetPaths.logoIcon),
            gap(height: 10),
            customText(
              text: "Product Name",
              fontWeight: FontWeight.bold,
              fontSize: 17,
              textColor: AppColors.primaryColor,
            ),
            gap(height: 10),
            infoItem(),
            gap(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                customText(
                  text: "Muhammad Zaid",
                  fontSize: 13,
                  textColor: AppColors.primaryColor,
                ),
                customText(
                  text: "PKR 1500",
                  fontSize: 13,
                  textColor: AppColors.redColor,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
            gap(height: 10),
            location(),
            gap(height: 30),
            customText(
              text: DummyData.dummyPara,
              textColor: AppColors.greyColor,
              fontWeight: FontWeight.normal,
              maxLines: 40,
              fontSize: 10,
            ),
          ],
        ),
      ),
    );
  }

  Widget location() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const Icon(
          Icons.location_on,
          color: AppColors.primaryColor,
          size: 22,
        ),
        gap(width: 5),
        customText(
          text: "Peshawar, Pakistan",
          textColor: AppColors.greyColor,
          fontWeight: FontWeight.normal,
          fontSize: 12,
        )
      ],
    );
  }

  Widget infoItem() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        color: Colors.grey.shade300,
        borderRadius: BorderRadius.circular(10),
      ),
      height: 50,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: customText(
              text: "0324 48093284",
              textAlign: TextAlign.center,
              textColor: AppColors.primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Icon(
                  Icons.phone,
                  color: AppColors.primaryColor,
                  size: 40,
                ),
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  height: 30,
                  width: 1,
                  color: AppColors.greyColor,
                ),
                const Icon(
                  Icons.whatsapp,
                  size: 40,
                  color: AppColors.primaryColor,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
