import 'package:flutter/material.dart';
import 'package:pakistani_app/screens/change_password.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/base_widget.dart';
import 'package:pakistani_app/widgets/gap.dart';
import 'package:pakistani_app/widgets/text.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      alignCenter: false,
      disableFab: false,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          topProfileIcon(context),
          gap(height: 10),
          customText(
            text: "Hanif Khan",
            textColor: AppColors.blackColor,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
          gap(height: 10),
          info(),
        ],
      ),
    );
  }

  Widget info() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        infoItem(icon: Icons.phone, title: "08908409324"),
        gap(height: 5),
        infoItem(icon: Icons.email, title: "hanifullah@gmail.com"),
      ],
    );
  }

  Widget infoItem({
    required IconData icon,
    required String title,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          size: 25,
          color: AppColors.blackColor,
        ),
        gap(width: 7),
        customText(
          text: title,
          fontSize: 14,
        ),
      ],
    );
  }

  Widget topProfileIcon(context) {
    return IntrinsicHeight(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(flex: 1, child: Container()),
          const Expanded(
              flex: 1,
              child: CircleAvatar(
                backgroundColor: AppColors.primaryColor,
                radius: 40,
              )),
          Expanded(
            flex: 1,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Icon(Icons.power_settings_new,
                    color: AppColors.redColor, size: 25),
                gap(height: 10),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChangePassword()));
                  },
                  child: const Icon(Icons.settings,
                      color: AppColors.blackColor, size: 25),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
