import 'package:flutter/material.dart';
import 'package:pakistani_app/controllers/main_controller.dart';
import 'package:pakistani_app/screens/homepage.dart';
import 'package:pakistani_app/screens/profile.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:provider/provider.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  List<Widget> pages = [
    const Homepage(),
    const Profile(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.lightGreen,
      bottomNavigationBar:
          Consumer<MyController>(builder: (context, provider, child) {
        return BottomNavigationBar(
          unselectedItemColor: Colors.black54,
          selectedItemColor: AppColors.primaryColor,
          currentIndex: provider.bottomNavBarIndex,
          backgroundColor: AppColors.lightGreen2,
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
            BottomNavigationBarItem(
                icon: Icon(Icons.dashboard), label: "Profile"),
          ],
          onTap: (int index) {
            provider.changeIndex(index);
          },
        );
      }),
      body: SafeArea(
        child: Consumer<MyController>(builder: (context, provider, child) {
          return Column(
            children: [
              Expanded(
                child: pages[provider.bottomNavBarIndex],
              )
            ],
          );
        }),
      ),
    );
  }
}
