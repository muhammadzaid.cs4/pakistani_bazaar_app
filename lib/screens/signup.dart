import 'package:flutter/material.dart';
import 'package:pakistani_app/config/asset_paths.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/app_button.dart';
import 'package:pakistani_app/widgets/base_widget.dart';
import 'package:pakistani_app/widgets/custom_textfield.dart';
import 'package:pakistani_app/widgets/gap.dart';
import 'package:pakistani_app/widgets/text.dart';
import 'package:provider/provider.dart';

import '../controllers/signup_controller.dart';

class Signup extends StatelessWidget {
  Signup({Key? key}) : super(key: key);

  final fullNameController = TextEditingController();
  final phoneNumber = TextEditingController();
  final password = TextEditingController();
  final email = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final signUpProvider = context.read<SignUpController>();
    return BaseWidget(
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    AssetPaths.logoIcon,
                    height: 70,
                  ),
                  gap(height: 15),
                  customText(
                    text: "Registration",
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                    textColor: AppColors.blackColor,
                  ),
                  gap(height: 10),
                  textField(
                    hintText: "Full Name",
                    controller: fullNameController,
                    leadingIcon: Icons.person,
                  ),
                  gap(height: 10),
                  textField(
                    hintText: "Phone Number like 03XXXXXXXX",
                    controller: phoneNumber,
                    leadingIcon: Icons.phone,
                  ),
                  gap(height: 10),
                  textField(
                    hintText: "Password",
                    controller: password,
                    leadingIcon: Icons.visibility,
                    enableSuffixIcon: true,
                  ),
                  gap(height: 10),
                  textField(
                    hintText: "Email (Optional)",
                    controller: email,
                    leadingIcon: Icons.email,
                  ),
                  gap(height: 10),
                  AppButton(
                      buttonText: "Register",
                      ontap: () {
                        signUpProvider.signUp(
                            phone: phoneNumber.text, password: password.text);
                      }),
                ],
              ),
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: customText(
              text: "Do you have an account? Login in",
              fontWeight: FontWeight.bold,
              textColor: AppColors.blackColor.withOpacity(0.7),
              fontSize: 14,
            ),
          )
        ],
      ),
    );
  }
}
