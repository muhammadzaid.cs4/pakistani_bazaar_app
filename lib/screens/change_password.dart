import 'package:flutter/material.dart';
import 'package:pakistani_app/widgets/app_button.dart';
import 'package:pakistani_app/widgets/base_widget.dart';
import 'package:pakistani_app/widgets/custom_textfield.dart';
import 'package:pakistani_app/widgets/gap.dart';
import 'package:pakistani_app/widgets/text.dart';

class ChangePassword extends StatelessWidget {
  ChangePassword({Key? key}) : super(key: key);

  final oldPassController = TextEditingController();
  final newPassController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          gap(height: 50),
          customText(
            text: "Change Password",
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
          gap(height: 10),
          textField(
            hintText: "Old Password",
            controller: oldPassController,
            leadingIcon: Icons.visibility,
            enableSuffixIcon: true,
          ),
          gap(height: 10),
          textField(
            hintText: "Old Password",
            controller: newPassController,
            leadingIcon: Icons.visibility,
            enableSuffixIcon: true,
          ),
          gap(height: 10),
          AppButton(buttonText: "Change Password", ontap: () {}),
        ],
      ),
    );
  }
}
