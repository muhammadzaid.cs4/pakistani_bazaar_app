import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pakistani_app/config/asset_paths.dart';
import 'package:pakistani_app/screens/app.dart';

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  moveToNextScreen(context) {
    Timer(const Duration(seconds: 3), () {
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => const App()),
        (Route<dynamic> route) => false,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    moveToNextScreen(context);
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage(AssetPaths.splashScreen),
          fit: BoxFit.cover,
        )),
      ),
    );
  }
}
