import 'package:flutter/material.dart';
import 'package:pakistani_app/config/asset_paths.dart';
import 'package:pakistani_app/controllers/main_controller.dart';
import 'package:pakistani_app/screens/signup.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/app_button.dart';
import 'package:pakistani_app/widgets/base_widget.dart';
import 'package:pakistani_app/widgets/gap.dart';
import 'package:pakistani_app/widgets/text.dart';
import 'package:provider/provider.dart';

import '../widgets/custom_textfield.dart';

class Login extends StatelessWidget {
  Login({Key? key}) : super(key: key);

  final TextEditingController phoneController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      alignCenter: false,
      child: Align(
        alignment: Alignment.topCenter,
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    gap(height: 7),
                    customText(
                      text: "Login",
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                    gap(height: 10),
                    loginLogo(),
                    gap(height: 10),
                    textField(
                      controller: phoneController,
                      hintText: "Phone number like 03XXXXXXX",
                      leadingIcon: Icons.phone,
                    ),
                    gap(height: 10),
                    textField(
                      controller: passwordController,
                      hintText: "Password",
                      leadingIcon: Icons.visibility,
                      enableSuffixIcon: true,
                    ),
                    gap(height: 10),
                    rememberMe(),
                    gap(height: 10),
                    AppButton(buttonText: "Login", ontap: () {}),
                  ],
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Signup()));
              },
              child: customText(
                text: "Not registered? Join Now!",
                fontWeight: FontWeight.bold,
                textColor: AppColors.blackColor.withOpacity(0.7),
                fontSize: 14,
              ),
            ),
            gap(height: 10),
          ],
        ),
      ),
    );
  }

  Widget rememberMe() {
    return Consumer<MyController>(builder: (context, provider, child) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Checkbox(
            activeColor: AppColors.primaryColor,
            value: provider.rememberMe,
            onChanged: (value) {
              provider.rememberMee(value);
            },
          ),
          customText(
            text: "Remember Me",
            textColor: AppColors.greyColor,
          ),
        ],
      );
    });
  }
 }

Widget loginLogo() {
  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        border: Border.all(
          color: Colors.grey.shade300,
        )),
    alignment: Alignment.center,
    width: 125,
    height: 125,
    child: Image.asset(
      AssetPaths.logoIcon,
      fit: BoxFit.cover,
    ),
  );
}
