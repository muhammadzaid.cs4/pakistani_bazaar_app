import 'package:flutter/material.dart';
import 'package:pakistani_app/data/dummy.dart';
import 'package:pakistani_app/screens/screen_category.dart';
import 'package:pakistani_app/screens/screen_detail.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/base_widget.dart';
import 'package:pakistani_app/widgets/gap.dart';
import 'package:pakistani_app/widgets/product_widget.dart';
import 'package:pakistani_app/widgets/text.dart';

double width = 0.0;
double height = 0.0;

class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    return BaseWidget(
      disableFab: false,
      alignCenter: false,
      appBartitle: "Pakistani Bazaar Marketplace",
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            gap(height: 8),
            topLocation(),
            gap(height: 20),
            textField(),
            gap(height: 18),
            topCategories(),
            gap(height: 18),
            buildProducts(
                items: [],
                title: "Mobile, Computer & Laptops",
                onProductTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ScreenDetail()));
                }),
            gap(height: 18),
            buildProducts(items: [], title: "Vehicles"),
            gap(height: 18),
            buildProducts(items: [], title: "Property/Real Estate"),
            gap(height: 18),
            buildProducts(items: [], title: "Furniture & Home Decor"),
            gap(height: 18),
            buildProducts(items: [], title: "Electronics & Home Appliances"),
            gap(height: 18),
            buildProducts(items: [], title: "Fashion & Beauty"),
          ],
        ),
      ),
    );
  }

  Widget topCategories() {
    List dummyData = DummyData.categories;
    return SizedBox(
      height: 100,
      child: ListView.builder(
          itemCount: dummyData.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return catItem(
                title: dummyData[index]["title"],
                iconPath: dummyData[index]["icon"]);
          }),
    );
  }

  Widget catItem({
    required String title,
    required String iconPath,
  }) {
    return SizedBox(
      width: 80,
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const ScreenCategory(),
            ),
          );
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              iconPath,
              height: 50,
              fit: BoxFit.fitHeight,
            ),
            gap(height: 5),
            customText(
              text: title,
              textColor: AppColors.redColor,
              fontSize: 10,
              maxLines: 2,
              textAlign: TextAlign.center,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
      ),
    );
  }

  buildProducts({
    required String title,
    required List items,
    VoidCallback? ontap,
    VoidCallback? onProductTap,
  }) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            customText(
              text: title,
              textColor: AppColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
            GestureDetector(
              onTap: ontap,
              child: customText(
                text: "View More",
                textColor: AppColors.redColor,
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
          ],
        ),
        gap(height: 4),
        SizedBox(
          height: height / 5,
          child: ListView.builder(
              itemCount: 5,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return GestureDetector(
                    onTap: onProductTap, child: const ProductWidget());
              }),
        ),
      ],
    );
  }

  Widget textField() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Expanded(
              child: TextField(
                decoration: InputDecoration(
                    border: InputBorder.none,
                    isCollapsed: true,
                    isDense: true,
                    hintText: "Search Product or Service",
                    hintStyle: TextStyle(
                      color: AppColors.greyColor,
                    )),
              ),
            ),
            const Icon(
              Icons.arrow_forward_ios,
              color: AppColors.blackColor,
              size: 15,
            )
          ],
        ),
        Divider(
          height: 2,
          color: AppColors.greyColor,
        )
      ],
    );
  }

  Widget topLocation() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const Icon(
          Icons.location_on,
          color: AppColors.primaryColor,
          size: 22,
        ),
        customText(
          text: "Select Location",
          textColor: AppColors.primaryColor,
          fontWeight: FontWeight.bold,
          fontSize: 15,
        )
      ],
    );
  }
}
