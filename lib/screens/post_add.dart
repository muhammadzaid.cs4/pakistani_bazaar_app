import 'package:flutter/material.dart';
import 'package:pakistani_app/widgets/app_button.dart';
import 'package:pakistani_app/widgets/base_widget.dart';
import 'package:pakistani_app/widgets/custom_textfield.dart';
import 'package:pakistani_app/widgets/gap.dart';

class PostAdd extends StatelessWidget {
  PostAdd({Key? key}) : super(key: key);

  final titleController = TextEditingController();
  final descController = TextEditingController();
  final contactController = TextEditingController();
  final priceController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          textField(
            hintText: "Post Title",
            controller: titleController,
            disableLeadingIcon: true,
          ),
          gap(height: 10),
          textField(
            hintText: "Post Description",
            controller: descController,
            disableLeadingIcon: true,
          ),
          gap(height: 10),
          textField(
            hintText: "Contact No",
            controller: contactController,
            disableLeadingIcon: true,
          ),
          gap(height: 10),
          textField(
            hintText: "Price",
            controller: priceController,
            disableLeadingIcon: true,
          ),
          gap(height: 10),
          AppButton(
            buttonText: "POST",
            ontap: () {},
          ),
        ],
      ),
    );
  }
}
