import 'package:flutter/material.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/text.dart';

class Loader extends StatelessWidget {
  final String? title;
  const Loader({Key? key, this.title = "Loading..."}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      height: double.infinity,
      color: Colors.grey.withOpacity(0.5),
      child: Container(
        width: 150,
        height: 150,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(9),
          color: Colors.grey,
        ), 
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/loader.gif',
              height: 50,
              fit: BoxFit.fitHeight,
            ),
            const SizedBox(
              height: 8,
            ),
            customText(
              text: title!,
              fontSize: 13,
              textAlign: TextAlign.center,
              maxLines: 2,
              textColor: AppColors.whiteColor,
            )
          ],
        ),
      ),
    );
  }
}
