import 'package:flutter/cupertino.dart';

Widget gap({
  double height = 0.0,
  double width = 0.0,
}) {
  return SizedBox(
    width: width,
    height: height,
  );
}
