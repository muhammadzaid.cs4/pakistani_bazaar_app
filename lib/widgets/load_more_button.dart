
import 'package:flutter/material.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/text.dart';

class LoadMoreButton extends StatefulWidget {
  final VoidCallback ontap;
  const LoadMoreButton({Key? key,required this.ontap}) : super(key: key);

  @override
  State<LoadMoreButton> createState() => _LoadMoreButtonState();
}

class _LoadMoreButtonState extends State<LoadMoreButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.ontap,
      borderRadius: BorderRadius.circular(9),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        width: 130,
        height: 50,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(9),
          color: AppColors.loadmorebuttonColor,
        ),
        child: customText(
          text: "Load More...",
          textColor: AppColors.whiteColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
