import 'package:flutter/material.dart';
import 'package:pakistani_app/controllers/main_controller.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/text.dart';
import 'package:provider/provider.dart';

Widget textField({
  required String hintText,
  required TextEditingController controller,
  IconData? leadingIcon,
  enableSuffixIcon = false,
  disableLeadingIcon = false,
}) {
  return Consumer<MyController>(builder: (context, provider, child) {
    return TextFormField(
      obscureText: enableSuffixIcon
          ? provider.isPasswordVisible
              ? true
              : false
          : false,
      cursorColor: AppColors.primaryColor,
      // obscureText: ,
      textAlignVertical: TextAlignVertical.center,
      style: const TextStyle(fontSize: 12),
      controller: controller,
      decoration: InputDecoration(
        // isDense: true,
        // contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        label: customText(text: hintText),
        fillColor: AppColors.shadowColor,
        prefixIcon: disableLeadingIcon ? null : Icon(leadingIcon, size: 24),
        filled: true,
        // hintText: hintText,
        suffixIcon: enableSuffixIcon
            ? Consumer<MyController>(builder: (context, provider, child) {
                return GestureDetector(
                    onTap: () {
                      provider.changeVisibility(!provider.isPasswordVisible);
                    },
                    child: Icon(
                      provider.isPasswordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ));
              })
            : const SizedBox(),
      ),
    );
  });
}
