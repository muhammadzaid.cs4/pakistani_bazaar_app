import 'package:flutter/material.dart';
import 'package:pakistani_app/config/strings.dart';
import 'package:pakistani_app/screens/homepage.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/gap.dart';
import 'package:pakistani_app/widgets/text.dart';

class ProductWidget extends StatelessWidget {
  const ProductWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 6),
      width: width / 3,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: AppColors.whiteColor,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: AppColors.greyColor,
              blurRadius: 6,
              spreadRadius: 1,
            ),
          ]),
      child: Column(
        children: [
          Expanded(
              flex: 6,
              child: Stack(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16),
                          topRight: Radius.circular(16),
                        ),
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(
                            AppStrings.imageUrl,
                          ),
                        )),
                  ),
                  Positioned(
                    top: 0,
                    right: 0,
                    child: lablel(color: AppColors.redColor, title: "9000/-"),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    child: lablel(
                        secondLable: true,
                        color: AppColors.greyColor,
                        title: "4 hours ago"),
                  ),
                ],
              )),
          Expanded(
              flex: 2,
              child: Container(
                color: AppColors.whiteColor,
                alignment: Alignment.center,
                child: customText(
                  text: "Samsung",
                  fontSize: 10,
                  fontWeight: FontWeight.bold,
                  textColor: AppColors.primaryColor,
                ),
              )),
          const Divider(height: 2, color: AppColors.blackColor),
          Expanded(
              flex: 2,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Icon(
                      Icons.location_on,
                      color: AppColors.redColor,
                      size: 14,
                    ),
                    gap(width: 4),
                    customText(
                      text: "Peshawar",
                      textColor: AppColors.greyColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 10,
                    )
                  ],
                ),
              )),
        ],
      ),
    );
  }

  Widget lablel(
      {required String title, required Color color, bool secondLable = false}) {
    return Container(
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.only(
          topRight: const Radius.circular(15),
          bottomLeft: secondLable ? Radius.zero : const Radius.circular(15),
        ),
      ),
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 5,
      ),
      child: customText(
        text: title,
        fontSize: 8,
        textColor: AppColors.whiteColor,
        fontWeight: FontWeight.normal,
      ),
    );
  }
}
