import 'package:flutter/material.dart';
import 'package:pakistani_app/screens/homepage.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/text.dart';

class AppButton extends StatelessWidget {
  final String buttonText;
  final VoidCallback ontap;
  const AppButton({Key? key, required this.buttonText, required this.ontap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ontap,
      child: Container(
        width: width,
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 13,
        ),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: AppColors.primaryColor,
        ),
        child: customText(
          text: buttonText,
          textColor: AppColors.whiteColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
