import 'package:flutter/material.dart';
import 'package:pakistani_app/screens/login.dart';
import 'package:pakistani_app/screens/post_add.dart';
import 'package:pakistani_app/screens/signup.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:pakistani_app/widgets/gap.dart';
import 'package:pakistani_app/widgets/text.dart';

class BaseWidget extends StatelessWidget {
  final Widget child;
  final String appBartitle;
  final bool disableAppBar;
  final bool disableFab;
  final bool alignCenter;
  const BaseWidget(
      {Key? key,
      required this.child,
      this.disableAppBar = false,
      this.alignCenter = true,
      this.disableFab = true,
      this.appBartitle = "Pakistani Bazaar Marketplace"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: disableAppBar
          ? null
          : AppBar(
              backgroundColor: AppColors.primaryColor,
              title: customText(
                text: appBartitle,
                textColor: AppColors.whiteColor,
                fontSize: 18,
              ),
            ),
      backgroundColor: AppColors.lightGreen,
      body: SafeArea(
        child: Container(
          alignment: alignCenter ? Alignment.center : Alignment.topLeft,
          padding: const EdgeInsets.only(
            left: 16,
            right: 16,
            top: 10,
          ),
          child: child,
        ),
      ),
      floatingActionButton: disableFab
          ? const SizedBox()
          : FloatingActionButton.extended(
              backgroundColor: AppColors.lightGreen,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Login(),
                  ),
                );
              },
              label: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Icon(
                    Icons.add,
                    color: AppColors.redColor,
                  ),
                  gap(width: 6),
                  customText(
                    text: "POST AD",
                    fontWeight: FontWeight.bold,
                    textColor: AppColors.redColor,
                  )
                ],
              ),
            ),
    );
  }
}
