import 'package:flutter/cupertino.dart';

import '../utils/colors.dart';

Text customText({
  required String text,
  double fontSize = 15.0,
  Color textColor = AppColors.textColor,
  FontWeight fontWeight = FontWeight.normal,
  int maxLines = 1,
  FontStyle fontStyle = FontStyle.normal,
  TextAlign textAlign = TextAlign.start,
}) {
  return Text(
    maxLines: maxLines,
    text,
    style: TextStyle(
      overflow: TextOverflow.ellipsis,
      fontStyle: fontStyle,
      fontSize: fontSize,
      color: textColor,
      fontWeight: fontWeight,

    ),
    textAlign: textAlign,
  );
}
