import 'package:flutter/material.dart';
import 'package:pakistani_app/utils/colors.dart';

BoxDecoration customboxDeco = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(15),
    boxShadow: [
      BoxShadow(
        spreadRadius: 2,
        blurRadius: 2,
        color: AppColors.shadowColor,
      ),
    ]);
