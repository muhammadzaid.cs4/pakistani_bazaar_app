import 'package:flutter/material.dart';

class AppColors {

  static const Color primaryColor = Color(0xff11782E);
  static const Color lightGreen2 = Color(0xffCAEEC6);
  static const Color textGreenColor = Color(0xff1C7034);
  static const Color redColor = Color(0xff941F15);
  static const Color blackColor = Colors.black;
  static Color lightGreen = const Color(0xffECF4EC);
  static Color whiteColor = Colors.grey.shade200;
  static const Color textColor = Colors.black;
  static Color shadowColor = Colors.grey.shade300;
  static Color greyColor = Colors.grey;
  static Color loadmorebuttonColor = const Color(0xff11782E);
}
