import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class MyController extends ChangeNotifier {
  int bottomNavBarIndex = 0;
  bool isPasswordVisible = false;
  bool rememberMe = false;

  changeIndex(index) {
    bottomNavBarIndex = index;
    notifyListeners();
  }

   rememberMee(value) {
    rememberMe =  value;
    notifyListeners();
  }

  changeVisibility(visible) {
    isPasswordVisible = visible;
    notifyListeners();
  }
  // BannerAd? bannerAd;

  // int bottomNavBarIndex = 0;
  // List jobs = [];
  // bool isImageDownloading = false;
  // bool isSearching = false;

  // int pageNumber = 1;
  // int pageNumberSearch = 1;
  // var searchKey = TextEditingController();
  // BannerAd? bannerAdDetail;
  // Future<void> launchMyUrl(url) async {
  //   if (!await launchUrl(
  //     url,
  //     mode: LaunchMode.externalApplication,
  //   )) {
  //     throw 'Could not launch $url';
  //   }
  // }

  // loadDetailBanner() {
  //   BannerAd(
  //     adUnitId: AdHelper.bannerAdUnitIdDetail,
  //     request: const AdRequest(),
  //     size: AdSize.banner,
  //     listener: BannerAdListener(
  //       onAdLoaded: (ad) {
  //         bannerAdDetail = ad as BannerAd;
  //         notifyListeners();
  //         print("Detail banner ad loaded");
  //       },
  //       onAdFailedToLoad: (ad, err) {
  //         if (kDebugMode) {
  //           print('Failed to load a banner ad: ${err.message}');
  //         }
  //         ad.dispose();
  //       },
  //     ),
  //   ).load();
  // }

  // loadBanner() {
  //   BannerAd(
  //     adUnitId: AdHelper.bannerAdUnitId,
  //     request: const AdRequest(),
  //     size: AdSize.banner,
  //     listener: BannerAdListener(
  //       onAdLoaded: (ad) {
  //         bannerAd = ad as BannerAd;
  //         notifyListeners();
  //       },
  //       onAdFailedToLoad: (ad, err) {
  //         if (kDebugMode) {
  //           print('Failed to load a banner ad: ${err.message}');
  //         }
  //         ad.dispose();
  //       },
  //     ),
  //   ).load();
  // }

  // @override
  // void dispose() {
  //   super.dispose();
  //   if (bannerAd != null) {
  //     bannerAd!.dispose();
  //   }
  //   if (bannerAdDetail != null) {
  //     bannerAdDetail!.dispose();
  //   }
  //   print("Disposed");
  // }

}
