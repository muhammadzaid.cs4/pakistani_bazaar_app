import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pakistani_app/controllers/main_controller.dart';
import 'package:pakistani_app/controllers/signup_controller.dart';
import 'package:pakistani_app/screens/splash.dart';
import 'package:pakistani_app/utils/colors.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: AppColors.primaryColor,
    systemNavigationBarColor: AppColors.primaryColor,
  ));
  runApp(const MyApp());
}

// testing
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => MyController()),
        ChangeNotifierProvider(create: (context) => SignUpController()),
      ],
      builder: (context, child) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Pakistani App',
        theme: ThemeData(
          colorScheme:
              ThemeData().colorScheme.copyWith(primary: AppColors.primaryColor),
          fontFamily: "PoppinsRegular",
          primaryColor: AppColors.primaryColor,
        ),
        home: const Splash(),
      ),
    );
  }
}
