import 'package:pakistani_app/config/asset_paths.dart';

class DummyData {
  static String dummyPara =
      "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.";
  static List<Map<String, dynamic>> categories = [
    {
      "title": "Mobiles, Computer & Laptops",
      "icon": AssetPaths.logoIcon,
    },
    {
      "title": "Vehicles",
      "icon": AssetPaths.logoIcon,
    },
    {
      "title": "Property for Sale",
      "icon": AssetPaths.logoIcon,
    },
    {
      "title": "Furniture & Decor",
      "icon": AssetPaths.logoIcon,
    },
    {
      "title": "Fashion and Beauty",
      "icon": AssetPaths.logoIcon,
    },
    {
      "title": "Bikes",
      "icon": AssetPaths.logoIcon,
    },
    {
      "title": "Electronics & Home Appliances",
      "icon": AssetPaths.logoIcon,
    },
  ];
}
